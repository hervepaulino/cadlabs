#include <som_cuda.hpp>

#include <exception>

using namespace std;

int main(int argc, char* argv[]) {

    if (argc < 5) {
        std::cerr << "usage " << argv[0] << " number_rows number_columns datafile outputfile [distance]\n";
        return 1;
    }

    try {
        cadlabs::som_cuda<float> s  (stoi(argv[1]), stoi(argv[2]),  string(argv[3]), string(argv[4]));
        s.run();
    }
    catch (runtime_error& e) {
        cerr << "Error processing file " << argv[3] << ": " << e.what() << "\n";
        return 1;
    }

    return 0;
}