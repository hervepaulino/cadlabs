//
// Created by Hervé Paulino on 2019-03-27.
//

#include <som.hpp>
#include <cmath>
#include <cstdlib>
#include <functional>


using namespace std;


namespace cadlabs {

    template<typename T = float>
    class som_seq : public som<T> {

        using Base = som<T>;

        using observation_type  = typename Base::observation_type;

        using value_type  = typename Base::value_type;

        const unsigned size;

        const std::function<float(observation_type&, int)>  distance_function;

    public:
        som_seq(const unsigned ncols, const unsigned nrows, string &&input_file, string &&output_file, string&& distance_func) :
                Base(ncols, nrows, std::move(input_file), std::move(output_file)),
                size(nrows * ncols),
                distance_function (distance_func == "e" ?
                                    bind(&som_seq::euclidean, this, placeholders::_1, placeholders::_2) :
                                    bind(&som_seq::cosine, this, placeholders::_1, placeholders::_2))
        { }

    private:
        inline unsigned index_of(unsigned row, unsigned col) const {
            return  row * this->number_cols + col;
        }


        float euclidean(observation_type &obs, int index) const {
            float result = 0;

            for (unsigned i = 0; i < this->number_features; i++) {
                auto val = (obs[i] - this->map[index+i]);
                result += val * val;
            }
            return sqrt(result);
        }

        float cosine(observation_type &obs, int index) const {
            float num = 0;
            float dem1 = 0;
            float dem2 = 0;

            for (unsigned i = 0; i < this->number_features; i++) {
                num += obs[i] * this->map[index+i];
                dem1 += obs[i];
                dem2 = this->map[index+i];
            }
            return 1 - ( num / (sqrt(dem1) * sqrt(dem2)));
        }

        unsigned arg_min(vector<float> &distances) const {
            unsigned result = 0;
            auto value = distances[0];

            for (unsigned i = 0; i < this->number_rows; i++) {
                for (unsigned j = 0; j < this->number_cols; j++) {
                    const auto index = index_of(i, j);
                    if (distances[index] < value) {
                        value = distances[index];
                        result = index;
                    }
                }
            }

            return result;
        }

        float neighborhood_function(unsigned bmu, unsigned current_point, unsigned npoints) {
            const float theta = (this->max_distance / 2.0f) - ((this->max_distance / 2.0f) * (this->iteration /
                    static_cast<float>(npoints)));
            const auto sqrDist = (bmu - current_point) * (bmu - current_point);

            return exp( -(sqrDist / (theta * theta)));
        }




        void update_map(observation_type &obs, unsigned bmu, unsigned npoints) {
            float learning_rate = 1 / static_cast<float>(this->iteration);


#ifdef DEBUG
            cadLog("learning_rate " << learning_rate)
#endif
            observation_type neighborhood(this->number_cols * this->number_rows);
            for (unsigned i = 0; i < this->number_rows; i++)
                for (unsigned j = 0; j < this->number_cols; j++) {
                    const auto index = index_of(i, j);
                    neighborhood[index] = neighborhood_function(bmu, index, npoints);
                }
#ifdef DEBUG
            cadLog("neighborhood " << neighborhood)
#endif

            for (unsigned i = 0; i < this->number_rows; i++)
                for (unsigned j = 0; j < this->number_cols; j+=this->number_features) {
                    const auto index = index_of(i, j);
                    for (unsigned f = 0; f < this->number_features; f++) {
                        if (neighborhood[index] > 0.01)
                            this->map[index + f] = this->map[index + f] +
                                              (learning_rate * neighborhood[index] * (obs[f] - this->map[index + f]));
                    }
                }
        }



        void process_observation(observation_type &obs) {
            this->iteration++;

#ifdef DEBUG
            cadLog("obs " << obs);
            cadLog("map " << this->map);
#endif
            observation_type distances(size);
            for (unsigned i = 0; i < this->number_rows; i++)
                for (unsigned j = 0; j < this->number_cols; j+=this->number_features) {
                    const auto index = index_of(i, j);

                    distances[index] = distance_function(obs, index);
                }

            auto bmu = arg_min(distances);

#ifdef DEBUG
            cadLog("distances " << distances);
            cadLog("bmu " << bmu);
#endif

            update_map(obs, bmu, this->dr.get_number_observations());
        }
    };
}

int main(int argc, char* argv[]) {

    if (argc < 5) {
        std::cerr << "usage " << argv[0] << " number_rows number_columns datafile outputfile [distance]\n";
        return 1;
    }

    try {
        if (argc == 5) {
            cadlabs::som_seq<float> s(stoi(argv[1]), stoi(argv[2]), string(argv[3]), string(argv[4]), "e");
            s.run();
        }
        else {
            cadlabs::som_seq<float> s(stoi(argv[1]), stoi(argv[2]), string(argv[3]), string(argv[4]), string(argv[5]));
            s.run();
        }

    }
    catch (std::runtime_error& e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}