#ifndef CADBLABS_SOM_HPP
#define CADBLABS_SOM_HPP

#include <cmath>
#include <iostream>

#include <data_reader.hpp>
#include <cadlabs.hpp>
#include <timer.hpp>

using namespace std;

namespace cadlabs {





    template <typename T = float>
    class som {

    public:
        using observation_type = typename data_reader<T>::observation_type;

        using value_type = typename data_reader<T>::value_type;

    protected:
        data_reader<T> dr;

        const unsigned number_cols;

        const unsigned number_rows;

        const unsigned number_features;

        vector<T> map;

        const float max_distance;

        unsigned iteration; // t

        const string output_file;


    public:
        som(const unsigned ncols, const unsigned nrows, string&& input_file, string&& output_file, unsigned seed = 0) :
                dr(input_file),
                number_cols(ncols),
                number_rows(nrows),
                number_features(dr.get_number_features()),
                map (number_cols * number_rows * number_features),
                max_distance(sqrt(nrows * nrows + ncols * ncols)),
                iteration(0),
                output_file (output_file) {

            const auto size = number_cols * number_rows * number_features;

            srand(seed);
            for (unsigned i = 0; i < size; i++)
                map[i] = static_cast<float> (rand()) / static_cast<float> (RAND_MAX);

        }

        void run() {

            cadlabs::timer<> t;
            t.start();
            while (1) {
                observation_type obs;
                dr >> obs;

                if (dr.eof())
                    break;

                process_observation(obs);
            }
            t.stop();

            t.print_stats(cout);
            cout << " milliseconds\n ";

            write_output();
        }

    protected:

        virtual void process_observation(observation_type& obs) {
            cadLog(obs);
        }

        void write_output() {
            ofstream os (output_file);

            for (unsigned i = 0; i < number_rows * number_cols * number_features; i++)
                os << map[i] << "\n";
        }
    };
}

#endif // CADBLABS_SOM_HPP