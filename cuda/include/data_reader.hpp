
#ifndef CADBLABS_DATA_READER_HPP
#define CADBLABS_DATA_READER_HPP

#include <vector>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

namespace cadlabs {


    template <typename T>
    class data_reader {

        ifstream is;

        unsigned number_observations;

        unsigned number_features;

    public:

        using value_type = T;

        using observation_type = vector<T>;

        data_reader(string& file_name) :
                number_observations (0),
                number_features (0){

            is.exceptions ( std::ifstream::failbit );
            is.open(file_name);

            // first read to obtain characteristics
            while (!eof()) {

                string line;
                try {
                    line = next_line();

                    if (eof()) // due to comment lines
                        break;
                }
                catch (std::ifstream::failure& e) {
                    break;
                }

                istringstream iss(line);
                string token;
                unsigned n = 0;
                while (getline(iss, token, ','))
                    n++;

                if (number_features == 0)
                    number_features = n;
                else if (number_features != n)
                    throw std::runtime_error("Error parsing file.");

                number_observations++;
            }

            // Reseting for the iterative reading
            is.clear();
            is.seekg(0);
        }


        ~data_reader() {
            is.close();
        }

        unsigned get_number_observations() const {
            return number_observations;
        }

        unsigned get_number_features() const {
            return number_features;
        }

        bool eof() const {
            return is.eof();
        }

        data_reader& operator>>(observation_type& obs) {

            if (!eof()) {
                try {
                    istringstream iss(next_line());

                    if (eof()) // due to comment lines
                        return *this;

                    string token;
                    while (getline(iss, token, ','))
                        obs.push_back(stod(token));
                }
                catch (std::ifstream::failure& e) {   }
            }

            return *this;
        }

    private:
        string next_line() {
            string line;
            do {
                if (is.eof())
                    return "";
                getline(is, line);
            } while (line == "" || line[0] == '%' || line[0] == '#');

            return line;
        }

    };
}

#endif //CADBLABS_DATA_READER_HPP
