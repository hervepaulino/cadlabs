#ifndef CADBLABS_SOM_CUDA_HPP
#define CADBLABS_SOM_CUDA_HPP

#include <som.hpp>

using namespace std;

namespace cadlabs {

    template <typename T>
    __global__ void distance(const T* map, const T* obs, float* result, const int number_features, const int map_size) {
        const int index = threadIdx.x + blockIdx.x * blockDim.x;



        if (index < map_size) {
            const int index_in_map = index * number_features;

            float res = 0;
            for (int i = 0; i < number_features; i++) {
                const T aux = (map[index_in_map + i] - obs[i]);
                res += aux * aux;
            }

            result[index]  = sqrt(res);
        }
    }

    template <typename T = float>
    class som_cuda : public som<T> {

        using Base = som<T>;

        using observation_type = typename Base::observation_type;

        const unsigned observation_size;

        static constexpr auto block_size = 512;


        T *map_gpu;

        T *obs_gpu;

        T *distance_gpu;


    public:
        som_cuda(const unsigned ncols, const unsigned nrows, string &&input_file, string &&output_file,
                 unsigned seed = 0) :
                Base(ncols, nrows, std::move(input_file), std::move(output_file), seed),
                observation_size(this->number_features * sizeof(T)) {

            const unsigned map_size = ncols * nrows * observation_size;

            test_error(cudaMalloc(&map_gpu, map_size));
            test_error(cudaMalloc(&obs_gpu, observation_size));
            test_error(cudaMalloc(&distance_gpu, ncols * nrows * sizeof(float)));
            test_error(cudaMemcpy(map_gpu, this->map.data(), map_size, cudaMemcpyHostToDevice));
        }


        ~som_cuda() {

            test_error(cudaFree(map_gpu));
            test_error(cudaFree(obs_gpu));
            test_error(cudaFree(distance_gpu));
        }

    protected:

        virtual void process_observation(observation_type &obs) {

            test_error(cudaMemcpy(obs_gpu, obs.data(), observation_size, cudaMemcpyHostToDevice));

            const auto size = this->number_cols * this->number_rows;
            const unsigned number_blocks = (this->number_rows * this->number_cols + block_size - 1) / block_size;

            distance << < number_blocks, block_size >> > (map_gpu, obs_gpu, distance_gpu, this->number_features, size);


#ifdef DEBUG
            vector<float> distances (size);
            test_error(cudaMemcpy(distances.data(), distance_gpu, size * sizeof(float), cudaMemcpyDeviceToHost));
            cadLog("distance to " << obs << ": " << distances << "\n");
#endif
        }

    private:
        void test_error(cudaError_t err) {
            if (err != cudaSuccess) {
                throw std::runtime_error(cudaGetErrorString(err));
            }
        }
    };
}

#endif // CADBLABS_SOM_CUDA_HPP