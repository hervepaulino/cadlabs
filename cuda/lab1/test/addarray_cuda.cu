#include <cadlabs.hpp>

using namespace std;

__global__ void add(const int* a, const int* b, int* c, const unsigned size) {
    //////////
    // TODO
    /////////
}


TEST(CUDA, ArrayAdd) {

    constexpr auto size = 1024;
    constexpr auto size_in_bytes = size * sizeof(int);

    array<int, size> a;
    fill(a.begin(), a.end(), 1);

    array<int, size> b;
    fill(b.begin(), b.end(), 2);

    array<int, size> c;

    //////////
    // TODO
    /////////

    array<int, size> expected;
    fill(expected.begin(), expected.end(), 3);
    expect_container_eq(c, expected);
}