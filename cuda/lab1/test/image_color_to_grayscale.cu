#include <cadlabs.hpp>
#include <wb.h>


__global__ void rgb2gray(float *grayImage, float *rgbImage, int channels,
                         int width, int height) {

  ////////////
  // TODO
  ////////////

}

TEST(CUDA, GrayScale) {

  const char *inputImageFile = ""; // TODO file to process";

  const wbImage_t inputImage = wbImport(inputImageFile);

  const int imageWidth  = wbImage_getWidth(inputImage);
  const int imageHeight = wbImage_getHeight(inputImage);

  // For this lab the value is always 3
  const int imageChannels = wbImage_getChannels(inputImage);

  // Since the image is monochromatic, it only contains one channel
  wbImage_t outputImage = wbImage_new(imageWidth, imageHeight, 1);

  float * hostInputImageData  = wbImage_getData(inputImage);
  float * hostOutputImageData = wbImage_getData(outputImage);


  ////////////
  // TODO
  ////////////



  wbImage_delete(outputImage);
  wbImage_delete(inputImage);

}
