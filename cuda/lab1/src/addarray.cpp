#include <cadlabs.hpp>
#include <timer.hpp>

using namespace std;


int main() {

    constexpr size_t size = 1024;

    array<int, size> a;
    fill(a.begin(), a.end(), 1);

    array<int, size> b;
    fill(b.begin(), b.end(), 2);

    array<int, size> c;

    cadlabs::timer<> t;
    t.start();
    for (int i = 0; i < size; i++)
        c[i] = a[i] + b[i];
    t.stop();

    cout << c << "\n";

    return 0;
}